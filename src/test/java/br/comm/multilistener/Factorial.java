package br.comm.multilistener;

import java.math.BigInteger;

import static br.comm.multilistener.TailCalls.call;
import static br.comm.multilistener.TailCalls.done;

public class Factorial {
    private static TailCall<BigInteger> calculate(final BigInteger factorial, final BigInteger number) {
        if (number.equals(BigInteger.ONE))
            return done(factorial);
        else
            return call(() -> calculate(factorial.multiply(number), number.subtract(BigInteger.ONE)));
    }

    public static BigInteger get(final BigInteger number) {
        return calculate(BigInteger.ONE, number).invoke();
    }
}
