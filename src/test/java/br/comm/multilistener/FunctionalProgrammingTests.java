package br.comm.multilistener;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class FunctionalProgrammingTests {
    private List<Item> items = new ArrayList<>();
    private static final Function<Item, Pair> geraPar = p -> new Pair(p.getCategoria(), p.getData());
    private static final Function<Item, Item> duplicaValor = item -> item.toBuilder().valor(item.getValor() * 2).build();

    @Before
    public void setup() {
        items.addAll(Arrays.asList(
                Item.builder().nome("a").categoria("casa").data(new Date(2020, 1, 1)).valor(10.0).build(),
                Item.builder().nome("b").categoria("casa").data(new Date(2020, 1, 1)).valor(20.0).build(),
                Item.builder().nome("c").categoria("escola").data(new Date(2020, 1, 1)).valor(30.0).build(),
                Item.builder().nome("d").categoria("escola").data(new Date(2020, 1, 1)).valor(40.0).build(),
                Item.builder().nome("e").categoria("casa").data(new Date(2020, 2, 1)).valor(50.0).build(),
                Item.builder().nome("f").categoria("casa").data(new Date(2020, 2, 1)).valor(60.0).build(),
                Item.builder().nome("g").categoria("escola").data(new Date(2020, 2, 1)).valor(70.0).build(),
                Item.builder().nome("h").categoria("escola").data(new Date(2020, 2, 1)).valor(80.0).build()
        ));
    }

    @Test
    public void grouping_and_summarize() {
        Map<Pair<String, Date>, Double> mapa = items.stream().
                collect(Collectors.groupingBy(p -> geraPar.apply(p),
                        Collectors.summingDouble(Item::getValor)));

        assertThat(mapa, allOf(hasKey(instanceOf(Pair.class)),
                               hasValue(instanceOf(Double.class))));
        System.out.println(mapa);
    }

    @Test
    public void grouping_in_lists() {
        Map<Pair<String, Date>, List<Item>> mapa = items.stream().
                collect(Collectors.groupingBy(p -> geraPar.apply(p),
                        toList()));

        assertThat(mapa, allOf(hasKey(instanceOf(Pair.class)),
                hasValue(instanceOf(List.class))));
        System.out.println(mapa);
    }

    @Test
    public void interface_function() {
        System.out.println(items);
        List<Item> nova = items.stream().map(duplicaValor).collect(toList());
        System.out.println(nova);
    }

    @Test
    public void reduce_with_multiple_interfaces_using_andThen() {
        Function<Integer, Integer> addOne = i -> ++i;
        Function<Integer, Integer> multiplyByTen = i -> 10*i;
        Function<Integer, Integer> multiplyByTwo = i -> 2*i;
        Function<Integer, Integer> result = Arrays.asList(addOne, multiplyByTen, multiplyByTwo).
                stream().reduce((h, next) -> h.andThen(next)).orElseGet(Function::identity);

        Integer t = result.apply(1);
        assertEquals(40l, t.longValue());
    }

    @Test
    public void composition_with_consumer() { 
        final Item item = Item.builder().build();
        Consumer<Item> addNome = i -> i.setNome("Adolfo");
        Consumer<Item> addCategoria = i -> i.setCategoria("Developer");
        Consumer<Item> addData = i -> i.setData(new Date());

        addNome.andThen(addCategoria).andThen(addData).accept(item);
        System.out.println(item);

    }


    @Test
    public void using_chain_of_responsibility() {
        Function<Integer, Integer> addOne = i -> ++i;
        Function<Integer, Integer> multiplyByTen = i -> 10*i;
        Function<Integer, Integer> multiplyByTwo = i -> 2*i;
        Function<Integer, Integer> chain = ChainOfResponsibility.handler(Arrays.asList(addOne, multiplyByTen, multiplyByTwo));

        assertEquals(40l, chain.apply(1).longValue());
    }


    @Test
    public void reduce_with_multiple_interfaces_using_compose() {
        Function<Integer, Integer> addOne = i -> ++i;
        Function<Integer, Integer> multiplyByTen = i -> 10*i;
        Function<Integer, Integer> multiplyByTwo = i -> 2*i;

        Function<Integer, Integer> result = Arrays.asList(addOne, multiplyByTen, multiplyByTwo).
                stream().reduce((h, next) -> h.compose(next)).orElseGet(Function::identity);

        Integer t = result.apply(1);
        assertEquals(21l, t.longValue());
    }

    @Test
    public void using_tail_recursion() {
        assertEquals(BigInteger.valueOf(120l), Factorial.get(BigInteger.valueOf(5)));
    }

    @Test
    public void max_profit_length_using_memoize() {
        System.out.println(this.maxProfit(22));
    }

    @Test
    public void fibonacci_using_stream() {
        int[] fibs = {0, 1};
        Stream<Integer> fibonacci = Stream.generate(() -> {
            int result = fibs[1];
            int fib3 = fibs[0] + fibs[1];
            fibs[0] = fibs[1];
            fibs[1] = fib3;
            return result;
        });

        System.out.println(fibonacci.limit(20l).collect(toList()));
    }

    private int maxProfit(final int rodLength) {
        List<Integer> prices = Arrays.asList(2,1,1,2,2,2,1,8,9,15);

        BiFunction<Function<Integer, Integer>, Integer, Integer> compute =
                (func, length) -> {
                    int profit = (length <= prices.size()) ? prices.get(length - 1)  : 0;
                    for (int i = 1; i < length; i++) {
                        int priceWhenCut = func.apply(i) + func.apply(length - i);
                        if (profit < priceWhenCut) profit = priceWhenCut;
                    }
                    return profit;
                };
        return Memoizer.call(compute, rodLength);
    }

}
