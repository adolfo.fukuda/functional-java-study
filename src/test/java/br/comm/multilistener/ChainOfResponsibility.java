package br.comm.multilistener;

import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.function.Function;

@UtilityClass
public class ChainOfResponsibility {
    Function handler(List<Function> elements) {
        return elements.stream().reduce((h, next) -> h.andThen(next)).orElseGet(Function::identity);
    }
}
